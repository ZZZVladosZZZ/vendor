<?php

namespace Task\First\Ui\DataProvider;

use Magento\Framework\App\ObjectManager;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\Request\DataPersistorInterface;
use Task\First\Model\Image\FileInfo;
use Task\First\Model\Shop as ShopModel;
use Task\First\Model\ResourceModel\Shop\Collection;
use Task\First\Model\ResourceModel\Shop\CollectionFactory;

class ShopDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    private $loadedData = [];

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /** @var ShopModel $shop */
        foreach ($items as $shop) {
            $this->loadedData[$shop->getId()] = $shop->getData();
        }

        $data = $this->dataPersistor->get('shop');
        if (!empty($data)) {
            $shop = $this->collection->getNewEmptyItem();
            $shop->setData($data);
            $this->loadedData[$shop->getId()] = $shop->getData();
            $this->dataPersistor->clear('shop');
        }

        return $this->loadedData;
    }

    /**
     * Converts image data to acceptable for rendering format
     *
     * @param \Task\First\Model\ResourceModel\Shop $shop
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return \Task\First\Model\ResourceModel\Shop $shop
     */
    private function convertValues(\Magento\Framework\Model\AbstractModel $shop)
    {
        $fileName = $shop->getImage();
        $image = [];
        if ($this->getFileInfo()->isExist($fileName)) {
            $stat = $this->getFileInfo()->getStat($fileName);
            $mime = $this->getFileInfo()->getMimeType($fileName);
            $image[0]['name'] = $fileName;
            $image[0]['url'] = $shop->getImageUrl();
            $image[0]['size'] = isset($stat) ? $stat['size'] : 0;
            $image[0]['type'] = $mime;
        }
        $shop->setImage($image);

        return $shop;
    }

    /**
     * Get FileInfo instance
     *
     * @return FileInfo
     */
    private function getFileInfo()
    {
        if ($this->fileInfo === null) {
            $this->fileInfo = ObjectManager::getInstance()->get(FileInfo::class);
        }
        return $this->fileInfo;
    }
}