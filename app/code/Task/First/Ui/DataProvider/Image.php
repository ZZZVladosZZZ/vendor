<?php

namespace Task\First\Ui\DataProvider;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\UrlInterface;
use Task\First\Model\ResourceModel\Shop;
use Magento\Store\Model\StoreManagerInterface;


class Image extends \Magento\Ui\Component\Listing\Columns\Column
{

    const ALT_FIELD = 'title';

    /**
     * Url path
     */
    const URL_PATH_EDIT = 'vendor/tmp/banners_slider';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Task\First\Model\Shop
     */
    protected $shop;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Task\First\Model\Shop $shop
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        StoreManagerInterface $storeManager,
        \Task\First\Model\Shop $shop,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
        $this->storeManager = $storeManager;
        $this->shop = $shop;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (!empty($dataSource['data']['items'])) {
            $fieldName = $this->getData('picture');
            foreach ($dataSource['data']['items'] as & $item) {
                $shop = new \Magento\Framework\DataObject($item);
                $item[$fieldName . '_src'] = $this->shop->getImageUrl($shop['image']);
                $item[$fieldName . '_orig_src'] = $this->shop->getImageUrl($shop['image']);
                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                    self::URL_PATH_EDIT,
                    ['entity_id' => $shop['entity_id']]
                );
                $item[$fieldName . '_alt'] = $shop['picture'];
            }
        }
        return $dataSource;
    }

//    const ALT_FIELD = 'title';
//
//    /**
//     * @var \Magento\Store\Model\StoreManagerInterface
//     */
//    protected $storeManager;
//
//    /**
//     * @var \Task\First\Ui\DataProvider\Image
//     */
//    protected $imageHelper;
//
//    /**
//     * @var UrlInterface
//     */
//    protected $urlBuilder;
//
//    /**
//     * @param ContextInterface $context
//     * @param UiComponentFactory $uiComponentFactory
//     * @param Image $imageHelper
//     * @param UrlInterface $urlBuilder
//     * @param StoreManagerInterface $storeManager
//     * @param array $components
//     * @param array $data
//     */
//    public function __construct(
//        ContextInterface $context,
//        UiComponentFactory $uiComponentFactory,
//        Image $imageHelper,
//        UrlInterface $urlBuilder,
//        StoreManagerInterface $storeManager,
//        array $components = [],
//        array $data = []
//    ) {
//        $this->storeManager = $storeManager;
//        $this->imageHelper = $imageHelper;
//        $this->urlBuilder = $urlBuilder;
//        parent::__construct($context, $uiComponentFactory, $components, $data);
//    }
//
//    /**
//     * Prepare Data Source
//     *
//     * @param array $dataSource
//     * @return array
//     */
//    public function prepareDataSource(array $dataSource)
//    {
//        if (isset($dataSource['data']['items'])) {
//            $fieldName = $this->getData('name');
//            foreach ($dataSource['data']['items'] as & $item) {
//                $url = '';
//                if ($item[$fieldName] != '') {
//                    $url = $this->storeManager->getStore()->getBaseUrl(
//                            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
//                        ) . 'vendor/vendor/emp' . $item[$fieldName];
//                }
//                $item[$fieldName . '_src'] = $url;
//                $item[$fieldName . '_alt'] = $this->getAlt($item) ?: '';
//                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
//                    'route/entity_id/edit',
//                    ['entity_id' => $item['entity_id']]
//                );
//                $item[$fieldName . '_orig_src'] = $url;
//            }
//        }
//
//        return $dataSource;
//    }
//
//    /**
//     * @param array $row
//     *
//     * @return null|string
//     */
//    protected function getAlt($row)
//    {
//        $altField = $this->getData('config/altField') ?: self::ALT_FIELD;
//        return isset($row[$altField]) ? $row[$altField] : null;
//    }
}
