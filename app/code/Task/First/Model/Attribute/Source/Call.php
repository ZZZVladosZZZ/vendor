<?php

namespace Task\First\Model\Attribute\Source;

class Call
{
    public function getShortCategoryDescription()
    {
        //return "working";
        return $this->_getAttribute('category_short_description');
    }
}