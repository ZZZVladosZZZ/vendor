<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Task\First\Model\Attribute\Source;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Task\First\Api\Data\ShopInterface;
use Task\First\Api\ShopRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\PageFactory;

class TakeFromDB extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Task_First::all';

    /**
     * @var ShopRepositoryInterface
     */
    protected $_shopFactory;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Realty constructor
     * @param  $shopFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        ShopRepositoryInterface $shopFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        PageFactory $resultPageFactory
    )
    {
        $this->_shopFactory = $shopFactory;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function getShopCollection()
    {
        $searchCriteriaBuilder = $this->_searchCriteriaBuilder->create();

        return $this->_shopFactory->getList($searchCriteriaBuilder)->getItems();
    }

    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        $_options = [];
        $var = $this->getShopCollection();
        if (!empty($var)) {
            foreach ($var as $item) {
                $_options[] = ['label' =>
                    $item->getName(),
                    'value' =>
                        $item->getId()
                ];

            }
        }


//            $this->_options = [];
//            $this->_options[] = ['label' => 'Label 1', 'value' => 'value 1'];
//            $this->_options[] = ['label' => 'Label 2', 'value' => 'value 2'];
//            $this->_options[] = ['label' => 'Label 3', 'value' => 'value 3'];
//            $this->_options[] = ['label' => 'Label 4', 'value' => 'value 4'];
//            $this->_options[] = ['label' => 'Label 5', 'value' => 'value 5'];

        return $_options;

    }
}