<?php

namespace Task\First\Model\ResourceModel\Shop;

use Task\First\Api\Data\ShopInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = ShopInterface::ENTITY_ID;
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
//        $this->_init(\Task\First\Model\Shop::class,
//            \Task\First\Model\ResourceModel\Shop::class);
        $this->_init('Task\First\Model\Shop',
            'Task\First\Model\ResourceModel\Shop');

    }
}
