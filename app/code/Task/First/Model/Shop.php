<?php

namespace Task\First\Model;


use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Task\First\Api\Data\ShopInterface;
use Magento\Framework\Model\AbstractModel;
use Task\First\Model\Image\FileInfo;

class Shop extends AbstractModel implements ShopInterface
{

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    protected function _construct()
    {
        $this->_init(\Task\First\Model\ResourceModel\Shop::class);
    }

    /**
     * @return int|mixed
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }
    /**
     * @return mixed|string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return mixed|string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }


    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID,$id);
    }

    /**
     * @param string $name
     * @return ShopInterface|Shop
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $description
     * @return ShopInterface|Shop
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION,$description);
    }

    private function _getStoreManager()
    {
        if ($this->_storeManager === null) {
            $this->_storeManager = ObjectManager::getInstance()->get(StoreManagerInterface::class);
        }
        return $this->_storeManager;
    }

    /**
     * Retrieve the Image URL
     *
     * @param string $imageName
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getImageUrl($imageName = null)
    {
        $url = '';
        $image = $imageName;
        if (!$image) {
            $image = $this->getData('image');
        }
        if ($image) {
            if (is_string($image)) {
                $url = $this->_getStoreManager()->getStore()->getBaseUrl(
                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                    ).FileInfo::ENTITY_MEDIA_PATH .'/'. $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
}
