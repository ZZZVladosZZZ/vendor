<?php

namespace Task\First\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Task\First\Api\Data;
use Task\First\Api\ShopRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Task\First\Model\ResourceModel\Shop as ResourceShop;
use Task\First\Model\ShopFactory as ShopFactory;
use Task\First\Model\ResourceModel\Shop\CollectionFactory as ShopCollectionFactory;



class ShopRepository implements ShopRepositoryInterface
{
    /**
     * @var ResourceShop
     */
    protected $resource;
    /**
     * @var ShopFactory
     */
    protected $shopFactory;
    /**
     * @var ShopCollectionFactory
     */
    protected $shopCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;
    /**
     * @var Data\ShopSearchResultsInterface
     */
    protected $searchResultsFactory;

    public function __construct(
        ResourceShop $resource,
        ShopFactory $shopFactory,
        ShopCollectionFactory $shopCollectionFactory,
        Data\ShopSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->resource = $resource;
        $this->shopFactory = $shopFactory;
        $this->shopCollectionFactory = $shopCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }


    /**
     * Save shop.
     *
     * @param \Task\First\Api\Data\ShopInterface $shop
     * @return \Task\First\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Task\First\Api\Data\ShopInterface $shop)
    {
        try {
            $this->resource->save($shop);

        } catch (\Exception $exception)
        {
            throw new CouldNotSaveException(
                __('Could not save the page: %1', $exception->getMessage()),
                $exception
            );
        }
        return $shop;
    }

    /**
     * Retrieve store.
     *
     * @param int $Id
     * @return \Task\First\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($Id)
    {
        $shop = $this->shopFactory->create();
        $this->resource->load($shop,$Id);
        if (!$shop->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $Id));
        }
        return $shop;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return Data\ShopSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->shopCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /**
         * @var Data\ShopSearchResultsInterface $searchResults
         */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete shop.
     *
     * @param \Task\First\Api\Data\ShopInterface $downhill
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Task\First\Api\Data\ShopInterface $downhill)
    {
        try {
            $this->resource->delete($downhill);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the page: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param int $shopId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($shopId)
    {
        return $this->delete($this->getById($shopId));
    }
}
