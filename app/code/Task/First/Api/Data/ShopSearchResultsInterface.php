<?php

namespace Task\First\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for cms page search results.
 * @api
 * @since 100.0.2
 */
interface ShopSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get pages list.
     *
     * @return \Task\First\Api\Data\ShopInterface[]
     */
    public function getItems();

    /**
     * Set pages list.
     *
     * @param \Task\First\Api\Data\ShopInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
