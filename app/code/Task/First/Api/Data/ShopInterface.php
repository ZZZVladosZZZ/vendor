<?php


namespace Task\First\Api\Data;


interface ShopInterface
{

    const ENTITY_ID                = 'entity_id';
    const NAME                     = 'name';
    const DESCRIPTION              = 'description';


    /**
     * @return int
     */
    public function getId();

    /**
     * Get Name
     *
     * @return string
     */
    public function getName();

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription();

    /**
     * @param $id
     * @return int
     */
    public function setId($id);
    /**
     * Get Name
     *
     * @param string $name
     * @return \Task\First\Api\Data\ShopInterface
     */
    public function setName($name);

    /**
     * Get Description
     *
     * @param string $description
     * @return \Task\First\Api\Data\ShopInterface
     */
    public function setDescription($description);


}