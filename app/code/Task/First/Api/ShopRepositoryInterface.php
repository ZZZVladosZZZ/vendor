<?php

namespace Task\First\Api;

interface ShopRepositoryInterface
{
    /**
     * Save store.
     *
     * @param \Task\First\Api\Data\ShopInterface $shop
     * @return \Task\First\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Task\First\Api\Data\ShopInterface $shop);

    /**
     * Retrieve shop.
     *
     * @param int $shopId
     * @return \Task\First\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($shopId);

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Task\First\Api\Data\ShopSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Task\First\Api\Data\ShopInterface $shop
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Task\First\Api\Data\ShopInterface $shop);

    /**
     * Delete store by ID.
     *
     * @param int $shopId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($shopId);
}
