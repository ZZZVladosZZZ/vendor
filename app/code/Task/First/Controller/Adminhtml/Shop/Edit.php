<?php
namespace Task\First\Controller\Adminhtml\Shop;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action;

/**
 * Edit Shop page action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->_objectManager->create('Task\First\Model\Shop');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This banner no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('vendor', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(
            'Task_First::all_banners'
        )->addBreadcrumb(
            __('Banners Slider'), __('Banners Slider')
        )->addBreadcrumb(
            __('All Banners'), __('All Banners')
        )->addBreadcrumb(
            $id ? __('Edit Banner') : __('New Banner'),
            $id ? __('Edit Banner') : __('New Banner')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('All Banners'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getName() : __('New Banner'));
        return $resultPage;
    }

    /**
     * Authorization level of a basic admin session
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Task_First::banner_read') || $this->_authorization->isAllowed('PHPCuong_BannerSlider::banner_create');
    }


//    /**
//     * Authorization level of a basic admin session
//     *
//     * @see _isAllowed()
//     */
//    const ADMIN_RESOURCE = 'Task_First::all';
//
//    /**
//     * Core registry
//     *
//     * @var \Magento\Framework\Registry
//     */
//    protected $_coreRegistry;
//
//    /**
//     * @var \Magento\Framework\View\Result\PageFactory
//     */
//    protected $resultPageFactory;
//
//    /**
//     * @param Action\Context $context
//     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
//     * @param \Magento\Framework\Registry $registry
//     */
//    public function __construct(
//        Action\Context $context,
//        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
//        \Magento\Framework\Registry $registry
//    ) {
//        $this->resultPageFactory = $resultPageFactory;
//        $this->_coreRegistry = $registry;
//        parent::__construct($context);
//    }
//
//    /**
//     * Init actions
//     *
//     * @return \Magento\Backend\Model\View\Result\Page
//     */
//    protected function _initAction()
//    {
//        // load layout, set active menu and breadcrumbs
//        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
//        $resultPage = $this->resultPageFactory->create();
//        $resultPage->setActiveMenu('Task_First::shop');
//        $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
//        $resultPage->addBreadcrumb(__('Manage Stores'), __('Manage Stores'));
//        return $resultPage;
//    }
//
//    /**
//     * Edit Store page
//     *
//     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
//     * @SuppressWarnings(PHPMD.NPathComplexity)
//     */
//    public function execute()
//    {
//        $resultPage = $this->_initAction();
//        $resultPage->addBreadcrumb(
//            __('Edit Store'), __('Edit Store')
//        );
//        return $resultPage;
//    }
}
