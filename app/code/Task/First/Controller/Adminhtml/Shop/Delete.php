<?php

namespace Task\First\Controller\Adminhtml\Shop;

use Magento\Backend\App\Action\Context;
use Task\First\Api\ShopRepositoryInterface;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Module_Work::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param ShopRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        ShopRepositoryInterface $shopRepository
    ) {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            $shop = $this->shopRepository->getById($id);
            try {
                $this->shopRepository->delete($shop);
                $this->messageManager->addSuccess(__("The shop was deleted success."));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__("Couldn't deleted the store."));
            }
        } else {
            $this->messageManager->addErrorMessage(__("Can't find the store."));
        }
        $this->_redirect("route/shop/index");
    }
}
