<?php

namespace Task\First\Controller\Adminhtml\Shop;

use Task\First\Api\Data\ShopInterfaceFactory;
use Task\First\Api\ShopRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Task\First\Api\Data\ShopInterface;
use Task\First\Model\Image\ImageUploader;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var ShopInterfaceFactory
     */
    private $shopFactory;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;
    /**
     * @var \Task\First\Model\Shop $url
     */
    public $url;

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * @param Context $context
     * @param ShopInterfaceFactory $shopFactory
     * @param ShopRepositoryInterface $shopRepository
     * @param DataPersistorInterface $dataPersistor
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        ShopInterfaceFactory $shopFactory,
        ShopRepositoryInterface $shopRepository,
        ImageUploader $imageUploader
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->shopFactory = $shopFactory;
        $this->imageUploader = $imageUploader;
        $this->shopRepository = $shopRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        /** @var ShopInterface $shop */
        $shop = $this->shopFactory->create();

        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
//            if (empty($data['entity_id'])) {
//                $data['entity_id'] = null;
//            }

            if (empty($data['entity_id'])) {
                $shop->setId($data['entity_id']);
            }

            $imageName = '';
            if (!empty($data['image'])) {
                $imageName = $data['image'][0]['picture'];
            }


            /** @var \Task\First\Model\Shop $model */
            $model = $this->_objectManager->create('Task\First\Model\Shop')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This banner no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                if ($imageName) {
                    $this->imageUploader->moveFileFromTmp($imageName);
                }
                $this->messageManager->addSuccess(__('You saved the banner.'));
                $this->dataPersistor->clear('route');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            }

            $this->dataPersistor->set('vendor', $data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Authorization level of a basic admin session
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Task_First::banner_update') || $this->_authorization->isAllowed('Task_First::banner_create');
    }


//    const ADMIN_RESOURCE = "Task_First::all";
//
//    /**
//     * @var ShopInterfaceFactory
//     */
//    private $shopFactory;
//
//    /**
//     * @var ShopRepositoryInterface
//     */
//    private $shopRepository;
//
//    /**
//     * @var DataPersistorInterface
//     */
//    protected $dataPersistor;
//
//    /**
//     * @var ImageUploader
//     */
//    protected $imageUploader;
//
//    /**
//     * @param Context $context
//     */
//
//    /**
//     * Constructor
//     *
//     * @param Context $context
//     * @param ShopInterfaceFactory $shopFactory
//     * @param ShopRepositoryInterface $shopRepository
//     * @param DataPersistorInterface $dataPersistor
//     * @param ImageUploader $imageUploader
//     */
//    public function __construct(
//        Context $context,
//        ShopInterfaceFactory $shopFactory,
//        ShopRepositoryInterface $shopRepository,
//        DataPersistorInterface $dataPersistor,
//        ImageUploader $imageUploader
//    )
//    {
//        parent::__construct($context);
//        $this->shopFactory = $shopFactory;
//        $this->shopRepository = $shopRepository;
//        $this->dataPersistor = $dataPersistor;
//        $this->imageUploader = $imageUploader;
//    }
//
//    public function execute()
//    {
//        $data = $this->getRequest()->getPostValue();
//        /** @var ShopInterface $shop */
//        $shop = $this->shopFactory->create();
//        if (!empty($data['entity_id']))
//            $shop->setId($data['entity_id']);
//        $shop
//            ->setName($data['name'])
//            ->setDescription($data['description']);
//        $this->shopRepository->save($shop);
//        $this->messageManager->addSuccess(__("The shop was saved success!!!!"));
//        $this->_redirect("route/shop/index");
//    }
//

}
