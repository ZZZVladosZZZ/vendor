<?php

namespace Task\First\Controller;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Task\First\Api\Data\ShopInterface;
use Task\First\Api\ShopRepositoryInterface;
use Magento\Framework\View\Element\Template;

class Index extends Template
{


    /**
     * @var ShopRepositoryInterface
     */
    protected $_shopFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Realty constructor
     * @param ShopRepositoryInterface $shopFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Template\Context $context
     */
    public function __construct(
        ShopRepositoryInterface $shopFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Template\Context $context
    )
    {
        $this->_shopFactory = $shopFactory;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;

        parent::__construct($context);
    }

    /**
     * @return ShopInterface[]
     * @throws LocalizedException
     */
    public function getShopCollection()
    {
        $searchCriteriaBuilder = $this->_searchCriteriaBuilder->create();

        return $this->_shopFactory->getList($searchCriteriaBuilder)->getItems();
    }

    /**
     * @return ShopInterface
     * @throws LocalizedException
     */
    public function getShop()
    {
        $id = $this->getRequest()->getParam('entity_id');

        return $this->_shopFactory->getById($id);
    }

}