<?php
/**
 * @author Elogic Team
 * @copyright Copyright (c) 2019 Elogic (https://elogic.co)
 */

namespace Task\First\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 *
 * @package Module\Work\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * @param EavSetup $eavSetup
     * @param QuoteSetupFactory $setupFactory
     */
    public function __construct(
        EavSetup $eavSetup
    )
    {
        $this->eavSetup = $eavSetup;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws Zend_Db_Exception
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'Downhill_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('shop_table')
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            [   'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            512,
            ['nullable' => false],
            'lox'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            1024,
            ['nullable'=> true]
        )>addIndex(
                $setup->getIdxName(
                    $installer->getTable('shop_table'),
                    ['picture'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['picture'],
                ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
            )->setComment('downhill for ever');

        $installer->getConnection()->createTable($table);
        $installer->endSetup();

    }
}
