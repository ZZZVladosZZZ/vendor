<?php
namespace Task\First\Block;

use Magento\Catalog\Model\Product;
use Task\First\Api\Data\ShopInterface;
use Magento\Framework\View\Element\Template;
use Task\First\Api\ShopRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;

class Create extends Template
{
    /**
     * @var ShopRepositoryInterface
     */
    protected $_shopFactory;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * Realty constructor
     * @param ShopRepositoryInterface $shopFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Template\Context $context
     */
    public function __construct(
        ShopRepositoryInterface $shopFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Registry $registry,
        Template\Context $context
    ) {
        $this->_shopFactory = $shopFactory;
        $this->registry = $registry;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;

        parent::__construct($context);
    }

    /**
     * @return ShopInterface[]
     * @throws LocalizedException
     */
    public function getShopCollection()
    {
        $searchCriteriaBuilder = $this->_searchCriteriaBuilder->create();

        return $this->_shopFactory->getList($searchCriteriaBuilder)->getItems();
    }

    /**
     * @return ShopInterface
     * @throws LocalizedException
     */
    public function getShop()
    {
        $id = $this->getRequest()->getParam('entity_id');

        return $this->_shopFactory->getById($id);
    }

    public function getVendorLogos()
    {
        $vendorIds = explode(',', $this->getVendorAttribute());

        $logoUrl = [];
        $i = 0;
        foreach ($vendorIds as $id){
            if (!empty($this->_shopFactory->getById($id)())){
                $logoUrl[$i]['url'] = $this->_shopFactory->getById($id)->getImageUrl();
                $logoUrl[$i]['alt'] = $this->_shopFactory->getById($id)->getName();
            }
            $i++;
        }

        return $logoUrl;
    }

    private function getVendorAttribute()
    {
        return $this->getData('vendor');
    }

    private function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }

    public function getProductName()
    {
        return $this->getProduct()->getName();
    }
}